package com.infyniteloop.ba.exceptions;

public class MissingHeaderException extends Exception{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MissingHeaderException() {
		super();
	}

	public MissingHeaderException(final String message) {
		super(message);
	}

}
